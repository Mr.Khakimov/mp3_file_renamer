import os
import eyed3
from joblib import Parallel, delayed

def remove_all_tags(file_path):
    # Загрузить MP3 файл
    audiofile = eyed3.load(file_path)

    # Удалить все теги
    if audiofile.tag:
        audiofile.tag.frame_set.clear()

        # Сохранить изменения
        audiofile.tag.save()

        print(f"Все теги удалены из файла: {file_path}")
    else:
        print(f"Файл не содержит тегов: {file_path}")

def remove_all_tags_in_directory(directory_path):
    # Получить список файлов в указанной папке
    files = [f for f in os.listdir(directory_path) if f.endswith(".mp3")]

    # Определить количество потоков (уровень параллелизма)
    num_threads = min(8, os.cpu_count() + 8)  # Вы можете настроить количество потоков

    # Использовать joblib для параллельного выполнения функции remove_all_tags
    Parallel(n_jobs=num_threads)(delayed(remove_all_tags)(os.path.join(directory_path, file_name)) for file_name in files)

if __name__ == "__main__":
    # Запросить путь к папке с MP3 файлами через консоль
    mp3_directory_path = input("Введите путь к папке с MP3 файлами: ")

    # Проверка существования папки
    if os.path.exists(mp3_directory_path):
        remove_all_tags_in_directory(mp3_directory_path)
    else:
        print(f"Папка не найдена: {mp3_directory_path}")
