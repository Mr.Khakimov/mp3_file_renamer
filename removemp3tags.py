import os
import eyed3
from concurrent.futures import ThreadPoolExecutor

def remove_all_tags(file_path):
    # Загрузить MP3 файл
    audiofile = eyed3.load(file_path)

    # Удалить все теги
    if audiofile.tag:
        audiofile.tag.frame_set.clear()

        # Сохранить изменения
        audiofile.tag.save()

        print(f"Все теги удалены из файла: {file_path}")
    else:
        print(f"Файл не содержит тегов: {file_path}")

def remove_all_tags_in_directory(directory_path):
    # Получить список файлов в указанной папке и всех подкаталогах
    mp3_files = []
    for root, dirs, files in os.walk(directory_path):
        mp3_files.extend([os.path.join(root, file) for file in files if file.endswith(".mp3")])

    # Определить количество потоков
    num_threads = min(8, os.cpu_count() + 4)  # Вы можете настроить количество потоков

    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        # Запустить удаление тегов в параллельных потоках
        executor.map(remove_all_tags, mp3_files)

if __name__ == "__main__":
    # Запросить путь к папке с MP3 файлами через консоль
    mp3_directory_path = input("Введите путь к папке с MP3 файлами: ")

    # Проверка существования папки
    if os.path.exists(mp3_directory_path):
        remove_all_tags_in_directory(mp3_directory_path)
    else:
        print(f"Папка не найдена: {mp3_directory_path}")