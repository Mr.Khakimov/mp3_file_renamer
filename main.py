import os
import eyed3


def remove_all_tags(file_path):
    # Загрузить MP3 файл
    audiofile = eyed3.load(file_path)

    # Удалить все теги
    if audiofile.tag:
        audiofile.tag.frame_set.clear()

        # Сохранить изменения
        audiofile.tag.save()

        print(f"Все теги удалены из файла: {file_path}")
    else:
        print(f"Файл не содержит тегов: {file_path}")


def rename_files_in_directory(directory_path):
    # Получить список файлов в указанной папке
    files = [f for f in os.listdir(directory_path) if f.endswith(".mp3")]

    # Перебрать все файлы, удалить теги и переименовать
    for index, file_name in enumerate(files, start=1):
        file_path = os.path.join(directory_path, file_name)

        # Удалить теги
        remove_all_tags(file_path)

        # Переименовать файл
        new_file_name = f"{index:03d}.mp3"  # Пример: 001.mp3, 002.mp3 и так далее
        new_file_path = os.path.join(directory_path, new_file_name)
        os.rename(file_path, new_file_path)

        print(f"Файл переименован: {file_path} -> {new_file_path}")


if __name__ == "__main__":
    # Укажите путь к вашей папке с MP3 файлами
    mp3_directory_path = "./"

    # Проверка существования папки
    if os.path.exists(mp3_directory_path):
        rename_files_in_directory(mp3_directory_path)
    else:
        print(f"Папка не найдена: {mp3_directory_path}")